from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker
from config import mysql_credentials

# URL to connection
url = 'mysql+pymysql://'+mysql_credentials['username']+':'+mysql_credentials['password']+'@localhost/'+mysql_credentials['db_name']

# Create an engine
engine = create_engine(url, convert_unicode = True)

# Container Objects
metadata = MetaData()

# Generated an Object Session: sessionmaker
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

def init_db():
	from resources import models
	metadata.create_all(bind=engine, checkfirst=True)
