import logging
import traceback
from flask import Blueprint
from config import swaggerUI 
from flask_restplus import Api
from sqlalchemy.orm.exc import NoResultFound

#-------- log -----------
log = logging.getLogger(__name__)

#------- blueprint --------
blueprint = Blueprint('api', __name__, url_prefix='/ecorooms')

api = Api(	blueprint, 
			title=swaggerUI['title'],
			version=swaggerUI['version'],
			description=swaggerUI['description'])

#------- error handler ------
@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)

@api.errorhandler(NoResultFound)
def database_not_found_error_handler(e):
    log.warning(traceback.format_exc())
    return {'message': 'A database result was required but none was found.'}, 404


