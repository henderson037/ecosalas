from sqlalchemy import Table, Column, Integer, DateTime, String
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Status(object):
	query = db_session.query_property()

	def __init__(self, title = None, description = None, s_type = None):

		self.title = title
		self.description = description
		self.type = s_type

	def __repr__():

		return '<Status title:{}, description:{}, type:{}>'.form(self.title, self.description, self.type)

status_table = Table('status', metadata,
					 Column('id', Integer, primary_key = True),
					 Column('title', String(50)),
					 Column('description', String(50)),
					 Column('code', Integer)
	                )

mapper(Status, status_table)