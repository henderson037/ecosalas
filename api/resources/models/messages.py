from sqlalchemy import Table, Column, Integer, DateTime, String
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Messages(object):
	query = db_session.query_property()

	def __init__(sefl, code = None, description_es = None, description_en = None, m_type = None):

		self.code = code
		self.description_es = description_es
		self.description_en = description_en
		self.type = m_type

	def __repr__(self):

		return '<Message code:{}, description_es:{}, description_en:{}, type:{}>'.format(self.code, self.description_es,self.description_en, self.type)


messages_table = Table('messages', metadata,
					   Column('id', Integer, primary_key=True),
					   Column('code', Integer),
					   Column('description_es', String(50)),
					   Column('description_en', String(50)),
					   Column('type',Integer)
					  )

mapper(Messages, messages_table)