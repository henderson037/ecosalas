from sqlalchemy import Table, Column, Integer, DateTime, String
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Login(object):
	query = db_session.query_property()

	def __init__(self, name = None, phone_number = None, user_email = None, password = None, avatar = None, company_id = None, profile_id = None,  status_id = None, start_attempts = None, created_at = None, updated_at = None, last_login = None):
		
		self.name = name
		self.user_name = user_name
		self.phone_number = phone_number
		self.user_email = user_email
		self.password = password
		self.avatar = avatar
		self.company_id = company_id
		self.profile_id = profile_id
		self.status_id = status_id
		self.start_attempts = start_attempts
		self.created_at = created_at
		self.updated_at = updated_at
		self.last_login = last_login

	def __repr__(self):
		return '<Login name:{}, user_email:{}, company_id:{}, profile_id:{}, status_id:{}, created_at:{}, last_login:{}>'.format(self.name, self.user_email, self.company_id, self.profile_id, self.status_id, self.created_at, self.last_login)

login_table = Table('login', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('name', String(50)),
                    Column('user_name', String(20)),
                    Column('phone_number', String(20)),
                    Column('user_email', String(30)),
                    Column('password', String(20)),
                    Column('avatar', String(60)),
                    Column('company_id', Integer),
                    Column('profile_id', Integer),
                    Column('status_id', Integer),
                    Column('start_attempts',Integer),
                    Column('created_at', DateTime),
                    Column('updated_at', DateTime),
                    Column('last_login', DateTime),
                    )

mapper(Login, login_table)