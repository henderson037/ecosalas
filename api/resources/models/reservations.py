from sqlalchemy import Table, Column, Integer, DateTime, Boolean
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Reservations(object):
	query = db_session.query_property()

	def __init__(self, date = None, start_date = None, end_date = None, confirmed = None, quantity_participant = None, user_id = None, meeting_room_id = None, status_id = None):
		
		self.date = date
		self.start_date = start_date
		self.end_date = end_date
		self.confirmed = confirmed
		self.quantity_participant = quantity_participant
		self.user_id = user_id
		self.meeting_room_id = meeting_room_id
		self.profile_id = profile_id
		self.status_id = status_id

	def __repr__(self):
		return '<Reservations date:{}, start_date:{}, end_date:{}, confirmed:{}, quantity_participant:{}, user_id:{}, meeting_room_id:{}, status_id:{} >'.format(self.date, self.start_date, self.end_date, self.confirmed, self.quantity_participant, self.user_id, self.meeting_room_id, self.status_id)

reservations_table = Table('reservations', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('date', DateTime),
                   	Column('start_date', DateTime),
                   	Column('end_date', DateTime),
                   	Column('confirmed', Boolean),
                   	Column('quantity_participant', Integer),
                   	Column('user_id', Integer),
                   	Column('meeting_room_id', Integer),
                   	Column('status_id', Integer),
                    )

mapper(Reservations, reservations_table)