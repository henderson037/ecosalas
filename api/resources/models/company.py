from sqlalchemy import Table, Column, Integer, DateTime, String
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Company(object):
	query = db_session.query_property()

	def __init__(self, company_name = None, company_type = None, company_id = None, avatar = None, created_at = None, updated_at = None, company_color = None, status_id = None):

			self.company_name = company_name, 
			self.company_type = company_type, 
			self.company_id = company_id, 
			self.avatar = avatar, 
			self.created_at = created_at,
			self.updated_at = updated_at, 
			self.company_color = company_color, 
			self.status_id = status_id

	def __repr__(self):
		return '<Company company_name:{}, company_type:{}, company_color:{}>'.format(self.company_name, self.company_type, self.company_color)

company_table = Table( 'company', metadata,
						Column('id', Integer, primary_key=True),
						Column('company_name', String(30)),
						Column('company_type', String(50)),
						Column('company_id', String(50)),
						Column('avatar', String(50)),
						Column('created_at', DateTime),
						Column('updated_at', DateTime),
						Column('company_color',String(50)),
						Column('status_id', Integer)
	                  )

mapper(Company, company_table)