from sqlalchemy import Table, Column, Integer, DateTime, String, Boolean
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Rooms(object):
	query = db_session.query_property()

	def __init__(self, name = None, resources = None, capacity = None, created_at = None, updated_at = None, priority = None, type_room_id = None, status_id = None):

			self.name = name, 
			self.resources = resources, 
			self.capacity = capacity, 
			self.created_at = created_at,
			self.updated_at = updated_at, 
			self.priority = priority, 
			self.type_room_id = type_room_id,
			self.status_id = status_id

	def __repr__(self):
		return '<Rooms name:{}, resources:{}, capacity:{}, created_at:{}, updated_at:{}, priority:{}, type_room_id:{}, status_id:{}>'.format(self.name, self.resources, self.capacity, self.created_at, self.updated_at, self.priority, self.type_room_id, self.status_id)

meetings_table = Table( 'meeting_room', metadata,
						Column('id', Integer, primary_key=True),
						Column('name', String(45)),
						Column('resources', String(250)),
						Column('capacity', Integer),
						Column('created_at', DateTime),
						Column('updated_at', DateTime),
						Column('priority', Boolean),
						Column('type_room_id',Integer),
						Column('status_id', Integer)
	                  )

mapper(Rooms, meetings_table)