from sqlalchemy import Table, Column, Integer, DateTime, String
from sqlalchemy.orm import mapper
from database import db_session, metadata

class Profile(object):
	query = db_session.query_property()

	def __init__(self, title = None, description = None, created_at = None, updated_at = None, status_id = None):	

		self.title = title
		self.description = description
		self.created_at = created_at
		self.updated_at = updated_at
		self.status_id = status_id

	def __repr__(self):

		return '<Profile title:{}, description:{}, status_id:{}>'.format(self.title, self.description, self.status_id)

profile_table = (Table('profile', metadata, 
				 Column('id', Integer, primary_key = True),
				 Column('title', String(50)),
				 Column('description', String(50)),
				 Column('created_at', DateTime),
				 Column('updated_at', DateTime),
				 Column('status_id', Integer)
				))

mapper(Profile, profile_table)