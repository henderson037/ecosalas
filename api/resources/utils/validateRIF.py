def digito_rif(ci):

    base = {'V': 1, 'E': 2, 'J': 3, 'P': 4, 'G': 5, 'C': 6}

    oper = [4, 3, 2, 7, 6, 5, 4, 3, 2]

    for i in range(len(ci[:9])):

        if i == 0:
            val = base.get(ci[0]) * 4
        else:
            val += oper[i] * int(ci[i])

    digit = 11 - (val % 11)

    if digit > 9:

        digit = 0  

    return ci[:9] + str(digit)