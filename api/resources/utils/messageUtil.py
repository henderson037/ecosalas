from resources.models.messages import Messages 

def get_message(code):

	message = Messages.query.filter_by(code=code).first()

	return {
			  "operation":False,
			  "data":{},
			  "message": {
						  "code": message.code,
						  "message_en": message.description_en,
						  "message_es": message.description_es
			  			}
			}
