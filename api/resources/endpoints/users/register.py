from restplus import api
from flask_restplus import Resource, fields
from resources.models.login import Login
from resources.utils import messageUtil
from database import db_session
import datetime

#-----------------------------------Users NameSpace -------------------------------------------
ns = api.namespace('user', description='NameSpace for operations Users.')
#----------- User Login --------------
e_register = api.model('user(registerPOST, registerPUT)', 	{
																'name':fields.String('name', required=True, pattern='(^[A-Za-z])', min_length=5, max_length=50),
																'username':fields.String('username', required=True, pattern='(^[A-Za-z])', min_length=5, max_length=20),
																'email':fields.String('email', required=True, pattern='(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', min_length=5, max_length=30),
										                        'password':fields.String('password', required=True, pattern='((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$%.-_!¡]))', min_length=8, max_length=20),
										                        'company':fields.String('company', required=True, pattern='(^[A-Za-z])', min_length=2, max_length=30),
										                        'avatar': fields.String('avatar'),
										                 	}
               				)

@ns.route('/register')
class register(Resource):

	#------------------------ POST ------------------------
	@ns.expect(e_register, validate=True)
	def post(self):

		args = ns.payload
		# Get Date
		actually_date = datetime.datetime.now()
		# Get register
		user_register = Login.query.filter_by(username=args['username']).first()
		# Get email
		user_email = Login.query.filter_by(user_email=args['email']).first()

		if user_register is None:
			if user_email is None:
				
				user_register = Login()
				user_register.name = args['name']
				user_register.username = args['username']
				user_register.phone_number = None
				user_register.user_email = args['email']
				user_register.password = args['password']
				user_register.avatar = None
				user_register.company_id = 1 # args['company']
				user_register.profile_id = 1
				user_register.status_id = 1
				user_register.start_attempts = 0
				user_register.created_at = actually_date
				user_register.update_at = actually_date

				# Add to table
				db_session.add(user_register)
				# Commit to DB
				db_session.commit()

				return {"response":{
							"operation":True,
							"data":{},
							"message": {}
						}
				},201

			else:

				return	{
							"response":messageUtil.get_message(1007)
					},200
		else:

			return	{
						"response":messageUtil.get_message(1006)
					},201



	#------------------------ PUT ------------------------
	@ns.expect(e_register, validate=True)
	def put(self):

		args = ns.payload
		#Get Date
		actually_date = datetime.datetime.now()
		# Get register
		user_update = Login.query.filter_by(username=args['username']).first()
		exists_email = Login.query.filter_by(user_email=args['email']).scalar() is not None

		if user_update is None:

			return {
						"response":messageUtil.get_message(1000)
				},200

		else:

			if exists_email:
				if user_update.user_email == args['email']:
					user_update.name = args['name']
					user_update.user_email = args['email']
					user_update.password = args['password']
					# Change to: args['company']
					user_update.company_id = 1
					user_update.avatar = args['avatar']
					user_update.update_at = actually_date

					# Commit to DB
					db_session.commit()

					return {"response":{
								"operation":True,
								"data":{},
								"message": {}
							}
					},201

				else:

					return {
								"response":messageUtil.get_message(1007)
						},200

			else:
				
				user_update.name = args['name']
				user_update.user_email = args['email']
				user_update.password = args['password']
				user_update.company_id = 1 # args['company']
				user_update.avatar = args['avatar']
				user_update.update_at = actually_date

				# Commit to DB
				db_session.commit()

				return {"response":{
							"operation":True,
							"data":{},
							"message": {}
						}
				},201