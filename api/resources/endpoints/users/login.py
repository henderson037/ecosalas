from restplus import api
from datetime import datetime, timedelta
from database import db_session
from flask_restplus import Resource, fields
from resources.models.login import Login 
from resources.models.company import Company 
from resources.models.profile import Profile 
from resources.utils import messageUtil
from dateutil.relativedelta import relativedelta

#-----------------------------------Users NameSpace --------------------------------------------
ns = api.namespace('user', description='NameSpace for Users operations.')
#--------- User Login Expect ----------
e_login = api.model('user(loginPOST, loginPUT)',{
							 'username' : fields.String('username', required=True, min_length=5, max_length=20),
	                         'password' : fields.String('password', required=True, pattern='((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$%.-_!¡]))', min_length=8, max_length=20)
	                         }
	                )
#----------- User Login --------------

@ns.route('/login')
class login(Resource):

	#------------------------ POST ------------------------
	@ns.expect(e_login, validate=True)
	def post(self):

		args = ns.payload

		user = Login.query.filter_by(user_name=args['username']).first()

		if user is None:

			return  {"response":messageUtil.get_message(1000)},200

		else:

			if (datetime.today() - relativedelta(months=3)) < user.last_login: 	#Validar contra la tabla de parametros

				if user.start_attempts < 3: 									#Validar contra la tabla de parametros

					if user.user_name == args['username'] and user.password == args['password']:

						user.start_attempts = 0
						db_session.commit()

						company = Company.query.get(user.company_id)
						profile = Profile.query.get(user.profile_id)

						return {"response":{
											  "operation":True,
											  "data":{	
											  			"user":user.user_name,
											  			"email":user.user_email,
														"avatar":user.avatar,
														"company":company.company_name,
														"profile":profile.title,
														"lastLogin":user.last_login.strftime('%Y-%m-%d')
											  		 },
											  "message": {}
											}
								},201

					else:

						user.start_attempts += 1
						db_session.commit()

						return {"response":messageUtil.get_message(1001),
								"user_attempts":user.start_attempts
								},200

				else:

					if user.status_id is not 3: #validar contra la tabla de status
						user.status_id = 3 		#validar contra la tabla de status
						db_session.commit()

					return {"response":messageUtil.get_message(1002)},200


			else:

				return  {"response":messageUtil.get_message(1003)},200

	#------------------------ PUT ------------------------
	@ns.expect(e_login, validate=True)
	def put(self):

		args = ns.payload

		user = Login.query.filter_by(user_name=args['username']).first()

		if user is None:

			return {"response":messageUtil.get_message(1000)},200
		else:

			if user.password == args['password']:
				
				return {"response":messageUtil.get_message(1004)},200

			else:

				user.password = args['password']
				user.start_attempts = 0
				user.status_id = 1
				db_session.commit()

				return {"response":{
									  "operation":True,
									  "data":{},
									  "message":{
									  				"message_en": "Password has been changed",
									  				"message_es": "Contraseña ha sido cambiada",
									  			}
									}
						},201
		


			
