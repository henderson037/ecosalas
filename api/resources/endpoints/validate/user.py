from restplus import api
from datetime import datetime
from flask_restplus import Resource, fields
from resources.models.login import Login
from resources.utils import messageUtil 

#-----------------------------------Users NameSpace --------------------------------------------
ns = api.namespace('validate', description='NameSpace for validate operations.')
#--------- Validate User Expect ----------
e_validate = api.model('validate(userPOST)',{
							 'email' : fields.String('email', required=True, pattern='(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', min_length=5, max_length=200)
	                         }
	                )
#----------- Validate User --------------

@ns.route('/user')
class user(Resource):

	@ns.expect(e_validate, validate=True)
	def post(self):

		args = ns.payload

		user = Login.query.filter_by(user_mail=args['email']).first()

		if user is None:
			return {"response":messageUtil.get_message(1000)},200
		else:

			return {"response":{
								  "operation":True,
								  "data":{
								  			"user":user.user_name,
								  			"email":user.user_email
								  		 },
								}
					},201