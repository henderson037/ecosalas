from restplus import api
from flask_restplus import Resource, fields
from resources.models.rooms import Rooms
from resources.utils import messageUtil
from database import db_session
import datetime

#-----------------------------------Users NameSpace -------------------------------------------
ns = api.namespace('rooms', description='NameSpace for operations Rooms.')
#----------- User Login --------------
s_register = api.model('rooms(registerPOST)', 	{
													'name':fields.String('name', required=True, pattern='(^[A-Za-z])', min_length=2, max_length=30),
													'capacity':fields.Integer('capacity', required=True, pattern='(^[0-9])'),
													'resources':fields.String('resources', required=True, pattern='(^[A-Za-z])', max_length=150),
													'type':fields.Integer('type', required=True, pattern='(^[0-9])'),
							                 	}
               				)

@ns.route('/register')
class register(Resource):

	#------------------------ POST ------------------------
	@ns.expect(s_register, validate=True)
	def post(self):

		args = ns.payload
		# Get Date
		actually_date = datetime.datetime.now()
		# Get register
		room_register = Rooms.query.filter_by(username=args['username']).first()
		# Get room name
		room_name = Rooms.query.filter_by(user_email=args['email']).first()

		if room_register is None:
			if room_name is None:
				
				room_register = Rooms()
				room_register.name = args['name']
				room_register.capacity = args['capacity']
				room_register.resources = args['resources']
				room_register.created_at = actually_date
				room_register.update_at = actually_date
				# Change to: args['priority']
				room_register.priority = 1
				# Change to: args['type']
				room_register.type_room_id = 1

				# Add to table
				db_session.add(room_register)
				# Commit to DB
				db_session.commit()

				return {"response":{
							"operation":True,
							"data":{},
							"message": {}
						}
				},201

			else:

				return	{
							"response":messageUtil.get_message(3001)
					},200
		else:

			return	{
						"response":messageUtil.get_message(3001)
					},201

