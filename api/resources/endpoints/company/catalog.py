from restplus import api
from database import db_session
from flask_restplus import Resource, fields
from resources.models.company import Company 
from resources.utils import messageUtil
from resources.utils import validateRIF
from resources.models.login import Login 

#-----------------------------------Company NameSpace --------------------------------------------
ns = api.namespace('company', description='NameSpace for Company operations.')
#--------- Company Catalog Expect ----------

e1_catalog = api.model('company(catalogPOST, catalogPUT)',{
											'name':fields.String('name', required = True, max_length = 30),
											'socialReason':fields.String('socialReason', required = True, max_length = 12),
											'color':fields.String('color', required = True),
											'avatar':fields.String('avatar', required=True)
										}
							  )

e2_catalog = api.model('company(catalogDELETE)',{
											'socialReason':fields.String('socialReason', required = True, max_length = 12),
										}
					  )

#--------- Company Catalog ----------

@ns.route('/catalog')
class catalog(Resource):

		#---------------------POST------------------------
		@ns.expect(e1_catalog, validate=True)
		def post(self):
			
			args = ns.payload

			rif = validateRIF.digito_rif(args['socialReason'])

			if rif == args['socialReason']:

				company = Company.query.filter_by(company_id = args['socialReason']).first()

				if company is None:

					company = Company()					
					company.company_name = args['name']
					company.company_id = args['socialReason']
					company.company_color = args['color']
					company.avatar = args['avatar']
					company.status_id = 2000 #Verificar con tabla status

					db_session.add(company)
					db_session.commit()

					return {"response":{
										"operation":True,
										"data":{},
										"message": {
														"message_en": "Company Registrared",
									  					"message_es": "Empresa registrada",
													}
										}
							},201

				else:

					return {"response":messageUtil.get_message(2000)},200

			else:

				return {"response":messageUtil.get_message(2001)},200

		#---------------------PUT------------------------
		@ns.expect(e1_catalog, validate=True)
		def put(self):

			args = ns.payload

			rif = validateRIF.digito_rif(args['socialReason'])

			if rif == args['socialReason']:

				company = Company.query.filter_by(company_id = args['socialReason']).first()

				if company is None:

					return {"response":messageUtil.get_message(2003)},200

				else:

					company.company_name = args['name']
					company.company_color = args['color']
					company.avatar = args['avatar']

					db_session.commit()
					
					return {"response":{
										"operation":True,
										"data":{},
										"message": {
														"message_en": "Company Updated",
									  					"message_es": "Empresa actualizada",
													}
										}
							},201
			else:

				return {"response":messageUtil.get_message(2001)},200

		#---------------------GET------------------------
		def get(self):
			
			companies = []

			company = Company.query.filter_by(code='2000').all() #2000 status active for company

			for row in company:

				companies.append({
									"id":row.id,
									"name":row.company_name,
									"socialReason":row.company_id,
									"color":row.company_color,
									"avatar":row.avatar,
									"status":row.status_id
								 })

			return {"response":{
									"operation":True,
									"data":companies,
									"message": {}
									}
						},201

		#---------------------DELETE------------------------
		@ns.expect(e2_catalog, validate=True)
		def delete(self):
			
			args = ns.payload

			rif = validateRIF.digito_rif(args['socialReason'])

			if rif == args['socialReason']:

				company = Company.query.filter_by(company_id = args['socialReason']).first()

				if company is None:

					return {"response":messageUtil.get_message(2003)},200

				else:

					users = Login.query.filter_by(company_id=company.id).all()

					for row in users:
						row.status_id = 1001 # status inactive for users

					company.status_id = 2001 # status inactive for company
					db_session.commit()


					return {"response":{
									"operation":True,
									"data":{},
									"message":{}
									}
						},201

			else:

				return {"response":messageUtil.get_message(2001)},200
			