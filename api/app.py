from flask import Flask
from restplus import blueprint
from resources import endpoints
from config import swaggerUI
from database import db_session


app = Flask(__name__)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

app.register_blueprint(blueprint)

if __name__ == "__main__":
	app.run(debug=True, port=swaggerUI["port"])